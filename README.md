---
title: "Notes: Thinking with Types, Type-Level Programming in Haskell by Sandy
Maguire"
---

# Contents

- [Chapter 01: The Algebra Behind Types](/01)
- [Chapter 02: Terms, Types and Kinds](/02)
- [Chapter 03: Variance](/03)
- [Chapter 04: Working with Types](/04)
- [Chapter 05: Constraints and GADTS](/05)
- [Chapter 06: Rank-N Types](/06)
- [Chapter 07: Existential Types](/07)
- [Chapter 08: Roles](/08)
- [Chapter 09: Associated Type Families](/09)
- [Chapter 10: First Class Families](/10)
- [Chapter 11: Extensible Data](/11)
- [Chapter 12: Custom Type Errors](/12)
- [Chapter 13: Generics](/13)
- [Chapter 14: Indexed Monads](/14)
- [Chapter 15: Dependent Types](/15)

